const Add = require('./index.js')
const assert = require('assert')

assert.equal(Add(1, 1), 2)
assert.equal(Add(1, 2), 3)
